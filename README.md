
# Proyecto Curso Intruduccion a Docker

# Unidad 2 - Proyecto Backend  Spring Boot Mongo y frontEnd


## 1 Backend
[Readme BackEnd](https://gitlab.com/public-unrn/curso-docker/-/blob/master/backend/README.md)

## 2 frontEnd
[Readme FrontEnd](https://gitlab.com/public-unrn/curso-docker/-/blob/master/frontend/README.md)


## 3 Docker Compose Backend + frontEnd + MongoDB
![Screenshot](compose.png)

```
version: '3.3'
services:
  curso-mongo:
    image: mongo:4.2.6
    volumes:
      - mongodb-cursos:/data/db
  curso-backend:
    build:
      context: backend
      dockerfile: Dockerfile
    environment:
      - SPRING_PROFILES_ACTIVE=docker
    ports:
      - '9010:8080'
    depends_on:
      - curso-mongo
  curso-frontend:
    build:
      context: frontend
      dockerfile: Dockerfile
    ports:
      - '9011:80'
    depends_on:
      - curso-backend

volumes:
  mongodb-cursos:
```


## 3.1 Docker-compose
```
docker-compose  up
docker-compose  -d up
docker-compose  down
docker-compose  -f docker-compose.yml -up
docker-compose build
```

Comandos de Docker compose

```
docker-compose ps
```

```
            Name                           Command               State            Ports          
-------------------------------------------------------------------------------------------------
curso-docker_curso-backend_1    java -Djava.security.egd=f ...   Up      0.0.0.0:9010->8080/tcp  
curso-docker_curso-frontend_1   nginx -g daemon off;             Up      0.0.0.0:9011->80/tcp    
curso-docker_curso-mongo_1      docker-entrypoint.sh mongod      Up      0.0.0.0:27017->27017/tcp
```


# Herramienta para Graficar Compose yml
```
docker run --rm -it --name dcv -v $(pwd):/input pmsipilot/docker-compose-viz render -m image --force docker-compose.yml --output-file=compose.png
```
