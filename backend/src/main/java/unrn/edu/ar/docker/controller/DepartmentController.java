package unrn.edu.ar.docker.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import unrn.edu.ar.docker.dto.DepartmentDTO;
import unrn.edu.ar.docker.mappers.DepartmentMapper;
import unrn.edu.ar.docker.mappers.EmployeeMapper;
import unrn.edu.ar.docker.model.Department;
import unrn.edu.ar.docker.repository.DepartmentRepository;
import unrn.edu.ar.docker.repository.EmployeeRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/department")
public class DepartmentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);

	@Autowired
	DepartmentRepository repository;

	@Autowired
    DepartmentMapper mapper;

	@Autowired
    EmployeeMapper employeeMapper;

	@Autowired
	EmployeeRepository employeeRepository;


	@PostMapping("/")
	public DepartmentDTO add(@RequestBody DepartmentDTO departmentDTO) {
		LOGGER.info("Department add: {}", departmentDTO);

		Department department = mapper.toEntity(departmentDTO);
		department = repository.save(department);

		return mapper.toDTO(department);
	}

	@GetMapping("/{id}")
	public DepartmentDTO findById(@PathVariable("id") String id) {
		LOGGER.info("Department find: id={}", id);
		Department department = repository.findById(id).orElse(null);
        return mapper.toDTO(department);
	}

	@GetMapping("/")
	public List<DepartmentDTO> findAll() {
		LOGGER.info("Department find");
		List<Department> departments = repository.findAll();

		return departments.stream().map(dept -> mapper.toDTO(dept)).collect(Collectors.toList());
	}

	@GetMapping("/find/{name}")
	public List<DepartmentDTO> findDepartmentsByName(@PathVariable("name") String name) {
		LOGGER.info("Department find: name={}", name);
		List<Department> departments =  repository.findDepartmentsByNameLike(name);

        return departments.stream().map(dept -> mapper.toDTO(dept)).collect(Collectors.toList());
	}

    @DeleteMapping("/{id}")
    public void deleteDepartment(@PathVariable("id") String id){
        LOGGER.info("Deleting..." + id);
        if (repository.findById(id).isPresent())
            repository.deleteById(id);
    }

}
